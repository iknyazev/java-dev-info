## Пет-проект

Проект - система учёта посещаемости и рабочего времени сотрудников.

#### Стадия 0

Для начала - сделать простое консольное приложение, которое будет хранить сотрудника, дату и время когда сотрудник пришёл на работу и ушёл с работы.  
Нужно функционал сделать вроде такого:  
- Можно посмотреть записи за дату. На ввод *дата*, на выходе список сотрудников, которые в этот день приходили на работу и соответствующее время прихода/ухода.
- Можно посмотреть статистику за временной промежуток. Например, за месяц. На ввод промежуток - например, *дата начала* и *дата конца*, или ***как считаешь нужным***. На выход - информация по каждому сотруднику за временной промежуток - сколько раз явился на работу, сколько часов пробыл и т.п.
- Можно посмотреть статистику по сотруднику за временной промежуток. На ввод *промежуток* и *уникальный идентификатор сотрудника* - на выход статистика по сотруднику, как в предыдущем пункте.
- Можно добавить запись по сотруднику. На ввод, например, получает *ФИО сотрудника* (или какой-то *уникальный идентификатор сотрудника* <-- **лучше вот это**), *дату*, *время прихода* и *время ухода*. На выход - либо ничего, либо запись, которая была создана.
- Можно удалить запись по сотруднику. На ввод *уникальный идентификатор сотрудника* и *дата*. На выход - либо ничего, либо информация, была ли удалена запись - например, **true** если запись была удалена, иначе **false**.

Использовать бд и т.п. не нужно, пока достаточно просто хранить их в оперативной памяти.

*(Опционально) Сохранять данные в файл какой-то .csv, .txt, .json, на твоё усмотрение, и читать его при запуске.*

*(Опционально) Добавить проверку на опоздание - например, если сотрудник пришёл позже 9:00 - то он опоздал.*

*(Опционально) Добавить учёт переработки - когда сотрудник за день пробыл в офисе больше 8 часов, то излишки записываются в переработку. Обрати внимание на то, как хранить время.*

*(Опционально) Использовать [Lombok](https://projectlombok.org/) и аннотации **Getter** и **Setter** для классов-контейнеров*

Spring, Hibernate и т.п. не нужно, начнём с чего-то простого и знакомого тебе.  
Сторонние библиотеки добавлять не нужно *(но и не запрещено* :) *)*

В качестве билд-тула Мавен или Градл, что хочешь - то и используй.

Сделай свой репозиторий в [**Гитлабе**](https://gitlab.com/) для проекта. Ветку **main**/**master** по возможности не трогай, создай ветку **dev**/**develop** и работай там. Как создашь, скинь мне в телеге.

Как только считаешь, что готово - отпишись.

Файлик этот можешь переименовать в *README.md* и закинуть к себе в репозиторий, так будет удобнее. 
