## План обучения

- **Build-tools** ([Maven](https://maven.apache.org/), [Gradle](https://gradle.org/)) – без фанатизма, достаточно узнать, как пользоваться, зачем это нужно. (резолв [зависимостей](https://mvnrepository.com/), jar-ники, mavenrepository и т.п.).
Интересные ресурсы:
    - https://habr.com/ru/articles/78252/ ← по мавену, но мало и не очень подробно
    - https://habr.com/ru/articles/77382/ ← тоже по мавену, подробно

- **[SQL](https://ru.wikipedia.org/wiki/SQL), [ORM](https://habr.com/ru/articles/237889/), [JPA](https://javarush.com/groups/posts/2259-jpa--znakomstvo-s-tekhnologiey)** – на уровне понимания, что это, зачем это и с чем его едят. [PostgreSQL](https://ru.wikipedia.org/wiki/PostgreSQL) в частности, как один из стандартов индустрии. А также драйверы для общения Java и [СУБД](https://habr.com/ru/companies/quadcode/articles/582136/) и [ORM](https://habr.com/ru/articles/237889/) – технологии, например, [Hibernate](https://hibernate.org/). Доскональное понимание и умение писать сложные запросы на 10 таблиц со всеми видами джойнов (пока) не требуется!  
Интересные ресурсы:
    - https://javarush.com/quests/QUEST_SQL_HIBERNATE_PUBLIC
    - https://www.w3schools.com/sql/
    - https://www.baeldung.com/learn-jpa-hibernate ← если ок с английским
    - https://vladmihalcea.com/tutorials/hibernate/ ← очень подробный и большой гайд, тоже на английском
    - https://habr.com/ru/articles/265061/ ← по jpa и hibernate

- **[HTTP](https://habr.com/ru/articles/215117/), [REST](https://habr.com/ru/articles/483202/), [MVC](https://habr.com/ru/articles/181772/)** – веб-программирование, что это такое, какие типичные инструменты, проблемы, use-кейсы. [Клиент-серверная архитектура](https://habr.com/ru/articles/495698/), как проиходит общение по интернету. Такие штуки, как [сервлеты](https://ru.wikipedia.org/wiki/Сервлет_(Java)), контейнеры сервлетов, [портлеты](https://habr.com/ru/articles/26316/) и т.п. (например, Apache Tomcat, JBoss, GlassFish) – достаточно просто знать, что это есть, изучать только по желанию.  
Интересные ресурсы:
    - https://habr.com/ru/articles/646651/ ← сервер приложения

- **[Паттерны проектирования](https://habr.com/ru/articles/84706/)** – зачем, почему, какие типы есть. Не обязательно уметь писать самостоятельно, нужно понимать принцип, какая именно проблема решается этим паттерном. Но все учить не надо, разумеется, их там оч много. Из важных и наиболее ходовых: 
    - [Singleton](https://javarush.com/groups/posts/589-patternih-i-singleton--dlja-vsekh-kto-vpervihe-s-nimi-stolknulsja) (для практики рекомендую написать самостоятельно, гайдов в интернете оооч много, этот паттерн буквально ВЕЗДЕ используется)
    - [Factory](https://javarush.com/groups/posts/2370-pattern-proektirovanija-factory) (если есть желание, можно написать)
    - [Builder](https://habr.com/ru/companies/otus/articles/552412/) (тоже можно самому написать, не оч сложно)
    - [Chain of Responsibility](https://habr.com/ru/articles/113995/) (достаточно почитать)
    - [Strategy](https://javarush.com/groups/posts/2271-pattern-proektirovanija-strategija)  
Интересные ресуры:
    - https://javarush.com/search/lecture?query=%D0%BF%D0%B0%D1%82%D1%82%D0%B5%D1%80%D0%BD%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F ← на жава гуру много контента по паттернам
    - https://refactoring.guru/ ← хороший сайт, но владелец украинец, поэтому доступа из РФ нет. Только через VPN

- **[Spring](https://spring.io/projects/spring-framework)** – что такое, зачем нужен, что умеет и т.п. Рекомендуется изучить предыдущие пункты. Из важного: [Beans](https://www.baeldung.com/spring-bean), [IoC](https://ru.wikipedia.org/wiki/Инверсия_управления), [Spring Context](https://docs.spring.io/spring-framework/docs/4.3.12.RELEASE/spring-framework-reference/html/overview.html), [Spring MVC](https://habr.com/ru/articles/336816/), [Spring Data JPA](https://habr.com/ru/articles/435114/). [Spring Boot](https://spring.io/projects/spring-boot) – что такое, чем отличается от просто Spring’а. Spring Configuration – достаточно будет изучить [Annotation-based](https://docs.spring.io/spring-framework/reference/core/beans/annotation-config.html) конфигурацию, XML можно не трогать. Аннотация @Autowired – что делает, откуда берёт бины, инжект бинов. Аннотация @Bean, @Component и наследники.
Интересные ресурсы:
    - https://habr.com/ru/articles/131993/ ← про IoC, DI и т.п.
    - https://habr.com/ru/articles/489236/ ← тоже про IoC, Spring ApplicationContext
    - https://habr.com/ru/articles/490586/ ← огромная статья на 40 минут, много, но не подробно
    - https://habr.com/ru/articles/435144/ ← микрогайд
    - https://www.baeldung.com/spring-tutorial ← много практических примеров, мало теории, нужен английский
    - https://spring-projects.ru/projects/spring-framework/ ← неоф дока на русском
    - https://habr.com/ru/articles/222579/ ← подробнее про контекст (но не слишком подробно)
    - https://habr.com/ru/companies/otus/articles/588378/ ← про конфигурацию и в целом про фреймворк
    - https://habr.com/ru/articles/661627/ ← сравнение и конвертация XML-конфига в Java-based (Annotation-based) конфиг
